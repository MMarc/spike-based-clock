import tree
import spike
import nt
import evoltree
import mcmc
import export
import numpy as np
import os
import graphics

# All output of this example will be stored in
folderName = 'InferenceOnSimulations'
os.mkdir(folderName)

# Our model parameters (kappa > 3*alpha)
tmax = 10
lamb, d = 1, 0.8
nu, kappa = 0.1, 0.04
alpha, beta = 0.02, 0.03
m = 2000

# either read a fixed tree
newick = '((((f0:0.581737,f1:0.581737)n:0.778417,(f2:0.27937,(f3:0.209972,f4:0.209972)n:0.069398)n:1.08078)n:1.1209,((f5:1.96728,((((f6:0.263772,f7:0.263772)n:0.226716,f8:0.490488)n:1.09481,((f9:0.210828,f10:0.210828)n:0.103948,f11:0.314775)n:1.27053)n:0.00682829,(((f12:0.740011,(f13:0.380064,f14:0.380064)n:0.359947)n:0.372974,f15:1.11298)n:0.225748,f16:1.33873)n:0.253396)n:0.375152)n:0.00431359,(f17:1.24321,((f18:0.0717595,f19:0.0717595)n:0.122457,f20:0.194217)n:1.04899)n:0.728384)n:0.509463)n:1.44333,(f21:1.6021,f22:1.6021)n:2.32229);'
t = tree.Tree(newick, format=1)
# or simulate randomly a birth-death tree
# t = tree.simBDtree(lamb, d, tmax)

# we specify a sampling fraction to each node of the tree
for n in t.traverse():
    n.add_features(sampf = 1)

# we simulate spikes under our model
t2 = spike.simBS(lamb, d, nu, t)

# we record the true params and spikes in a first file
os.chdir(folderName)
with open("true_params.txt", "a") as fichier:
    fichier.write("lamb:" + str(lamb) + '\n')
    fichier.write("d:" + str(d) + '\n')
    fichier.write("nu:" + str(nu) + '\n')
    fichier.write("kappa:" + str(kappa) + '\n')
    fichier.write("alpha:" + str(alpha) + '\n')
    fichier.write("beta:" + str(beta) + '\n')
with open("true_tree_without_spikes.txt", "a") as fichier:
    fichier.write("t:" + newick + '\n')
s2 = ""
for nd in t2.iter_descendants("postorder"):
    s2 += str(nd.spike) + " "
with open("true_spikes.txt", "a") as fichier:
    fichier.write(s2 + '\n')
with open("true_general_infos.txt", "a") as fichier:
    fichier.write('Inferences on simulations, with fixed tree.\n')
    fichier.write('Spikes are simulated, sequences (m='+str(m)+' nt) are simulated on the spiked tree.\n')
    fichier.write('And (lamb, d, nu, kappa, alpha, beta, spikes) are inferred using a MCMC with the AS2 moves.')
os.chdir('..')

# then we simulate sequences along the spiked tree
t3 = evoltree.simSeq([alpha, beta, kappa], t2, m)
# we get the alignment and simplify it
names, al = evoltree.getAlignment(t3)
simplifiedAl, columnCounts = nt.simplifyAlignment(al, nt.cmpColumnsBasic)

# run the mcmc
niter, nburnin, nsamp = 100000, 0, 1000
mcmc.mcmc(t, simplifiedAl, columnCounts, niter, nburnin, nsamp, folderName)

# we plot the histograms of parameter inferences
graphics.analyzeMcmc(lamb, d, nu, kappa, alpha, beta, t2, folderName)

