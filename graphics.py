from ete3 import Tree, TreeStyle, NodeStyle, faces, CircleFace
import pickle
import matplotlib.pyplot as plt
import os
import numpy as np

def layout_for_spikes(node):
    # We plot spikes as nodes. 
    # Other nodes are no longer represented.
    if "spike" in node.features:
        node.img_style['size'] = 5*node.spike
        node.img_style['shape'] = 'sphere'
        node.img_style['fgcolor'] = '#ff4500'
    else:
        node.img_style['size'] = 0

    # We plot the marginal support for each node as an additional sphere
    if 'spike_marginal_weight' in node.features:
        C = CircleFace(radius = 5*node.spike_marginal_weight, style = 'sphere', color = '#ffd700')
        C.opacity = 0.7
        faces.add_face_to_node(C, node, 0, position='float')
    

ts_with_spikes = TreeStyle()
ts_with_spikes.layout_fn = layout_for_spikes


def analyzeTreeOutput(fileName, trueTree):
    ''' we copy the true tree to have the right spike positions
    and we add a specific feature to count the number of spikes
    on each node on the inference output.'''

    # We load the results as an array
    a = np.loadtxt(fileName)

    # We average over the lines of the array
    mean_spikes = np.mean(a, axis=0)

    # We place these average numbers along the tree
    t = trueTree.copy()
    nn = 0
    for n in t.iter_descendants("postorder"):
        n.add_features(spike_marginal_weight = mean_spikes[nn])
        nn += 1

    # we plot the tree with true spikes and inferred ones,
    # and we return it
    t.show(tree_style = ts_with_spikes)
    return t


def analyzeParamOutput(paramList, paramName, trueValue):
    '''A simple histogram of inferred values, together with
    a red line showing the trueValue.'''
    plt.hist(paramList, normed=True)
    plt.ylabel('Posterior density')
    plt.xlabel(paramName)
    if trueValue != 0:
        plt.vlines(trueValue, plt.ylim()[0], plt.ylim()[1], color='red')
    return ''

def analyzeTrace(paramList, nList, ylabel):
    plt.plot(nList, paramList)
    plt.xlabel('iteration number')
    plt.ylabel(ylabel)
    return ''

def analyzeMcmc(trueLamb, trueD, trueNu, trueKappa, trueAlpha, trueBeta, trueTree, folderName):
    '''Wrapping together the histograms of parameter inferences, and
    the spike inferences. Function intended to be used at the end of an mcmc.'''
    os.chdir(folderName)
    a = np.loadtxt("result_params.txt", skiprows=1)
    llList = a[:,0]
    nList = a[:,1]
    acceptList = a[:,2]
    lambList = a[:,3]
    dList = a[:,4]
    nuList = a[:,5]
    kappaList = a[:,6]
    alphaList = a[:,7]
    betaList = a[:,8]

    plt.subplot(4,4,1)
    analyzeTrace(llList, nList, "log likelihood")
    plt.subplot(4,4,2)
    plt.hist(llList, normed=True)
    plt.ylabel('density')

    plt.subplot(4,4,3)
    analyzeTrace(acceptList, nList, "acceptation")
    plt.subplot(4,4,4)
    plt.hist(acceptList, normed=True)
    plt.ylabel('density')

    plt.subplot(4,4,5)
    analyzeTrace(lambList, nList, "lambda")
    plt.subplot(4,4,6)
    analyzeParamOutput(lambList, "lambda", trueLamb)

    plt.subplot(4,4,7)
    analyzeTrace(dList, nList, "d")
    plt.subplot(4,4,8)
    analyzeParamOutput(dList, "d", trueD)

    plt.subplot(4,4,9)
    analyzeTrace(nuList, nList, "nu")
    plt.subplot(4,4,10)
    analyzeParamOutput(nuList, "nu", trueNu)
    
    plt.subplot(4,4,11)
    analyzeTrace(kappaList, nList, "kappa")
    plt.subplot(4,4,12)
    analyzeParamOutput(kappaList, "kappa", trueKappa)
    
    plt.subplot(4,4,13)
    analyzeTrace(alphaList, nList, "alpha")
    plt.subplot(4,4,14)
    analyzeParamOutput(alphaList, "alpha", trueAlpha)
    
    plt.subplot(4,4,15)
    analyzeTrace(betaList, nList, "beta")
    plt.subplot(4,4,16)
    analyzeParamOutput(betaList, "beta", trueBeta)
    
    plt.show()

    analyzeTreeOutput("result_spikes.txt", trueTree)
    os.chdir("..")
    return ''

def analyzeMcmc3pos(trueLamb, trueD, trueNu, trueKappa1, trueKappa2, trueKappa3, trueAlpha, trueBeta, trueU, trueV, trueTree, folderName):
    '''Wrapping together the histograms of parameter inferences, and
    the spike inferences. Function intended to be used at the end of an mcmc.'''
    os.chdir(folderName)
    a = np.loadtxt("result_params.txt", skiprows=1)
    llList = a[:,0]
    nList = a[:,1]
    acceptList = a[:,2]
    lambList = a[:,3]
    dList = a[:,4]
    nuList = a[:,5]
    kappa1List = a[:,6]
    kappa2List = a[:,7]
    kappa3List = a[:,8]
    alphaList = a[:,9]
    betaList = a[:,10]
    uList = a[:,11]
    vList = a[:,12]

    plt.subplot(6,4,1)
    analyzeTrace(llList, nList, "log likelihood")
    plt.subplot(6,4,2)
    plt.hist(llList, normed=True)
    plt.ylabel('density')

    plt.subplot(6,4,3)
    analyzeTrace(acceptList, nList, "acceptation")
    plt.subplot(6,4,4)
    plt.hist(acceptList, normed=True)
    plt.ylabel('density')

    plt.subplot(6,4,5)
    analyzeTrace(lambList, nList, "lambda")
    plt.subplot(6,4,6)
    analyzeParamOutput(lambList, "lambda", trueLamb)

    plt.subplot(6,4,7)
    analyzeTrace(dList, nList, "d")
    plt.subplot(6,4,8)
    analyzeParamOutput(dList, "d", trueD)

    plt.subplot(6,4,9)
    analyzeTrace(nuList, nList, "nu")
    plt.subplot(6,4,10)
    analyzeParamOutput(nuList, "nu", trueNu)
    
    plt.subplot(6,4,11)
    analyzeTrace(kappa1List, nList, "kappa1")
    plt.subplot(6,4,12)
    analyzeParamOutput(kappa1List, "kappa1", trueKappa1)
    
    plt.subplot(6,4,13)
    analyzeTrace(kappa2List, nList, "kappa2")
    plt.subplot(6,4,14)
    analyzeParamOutput(kappa2List, "kappa2", trueKappa2)
    
    plt.subplot(6,4,15)
    analyzeTrace(kappa3List, nList, "kappa3")
    plt.subplot(6,4,16)
    analyzeParamOutput(kappa3List, "kappa3", trueKappa3)
    
    plt.subplot(6,4,17)
    analyzeTrace(alphaList, nList, "alpha")
    plt.subplot(6,4,18)
    analyzeParamOutput(alphaList, "alpha", trueAlpha)
    
    plt.subplot(6,4,19)
    analyzeTrace(betaList, nList, "beta")
    plt.subplot(6,4,20)
    analyzeParamOutput(betaList, "beta", trueBeta)
    
    plt.subplot(6,4,21)
    analyzeTrace(uList, nList, "u")
    plt.subplot(6,4,22)
    analyzeParamOutput(uList, "u", trueU)
    
    plt.subplot(6,4,23)
    analyzeTrace(vList, nList, "v")
    plt.subplot(6,4,24)
    analyzeParamOutput(vList, "v", trueV)
    
    plt.show()

    analyzeTreeOutput("result_spikes.txt", trueTree)
    os.chdir("..")
    return ''

