import tree
import spike
import nt
import evoltree
import numpy as np
from scipy.stats import norm
from math import exp, log, floor
import os

#    Note that parameter lambda is often called "b" in this file, because
#    this name changed late in the writing process. However, all outputs
#    should be consistent with "lambda".

def logPdfNormInX1X2(x, mu, sigma, x1, x2):
    '''Log of the probability density function of a random variable
    following a Normal law conditioned on staying on [x1,x2].'''
    return norm.pdf(x, mu, sigma) / (norm.cdf(x2, mu, sigma) - norm.cdf(x1, mu, sigma))

def simNormInX1X2(mu, sigma, x1, x2):
    '''Random simulation of a variable following a Normal distribution
    conditioned on staying on [x1,x2].'''
    x = norm.rvs(mu, sigma)
    while x > x2 or x < x1:
        x = norm.rvs(mu, sigma)
    return x

def mcmc(initTree, simplifiedAl, columnCounts, niter, nburnin, nsamp, folderName, verbose=False):
    '''This Markov Chain samples the posterior distribution
    of spikes along the tree, together with (b, d, nu, alpha, kappa)
    posterior values.
    Moves can be changed (from what I understand, it is mainly 
    a matter of finding the best cooking recipe by chance). To
    this end, I tried to highlight in the following code how changes
    can be made. '''
                
    # chain initialization for parameter values
    # possible changes : do whatever you want there.
    d = simNormInX1X2(0.8, 0.1, 0, 1)
    bmd = simNormInX1X2(0.1, 0.1, 0, 1)
    b = d+bmd
    nu = simNormInX1X2(0.1, 0.1, 0, 1)
    kappa = simNormInX1X2(0.1, 0.1, 0, 1)
    alpha = simNormInX1X2(0.01, 0.01, 0, 0.1)
    beta = simNormInX1X2(0.02, 0.01, 0, 0.1)
    
    # standard deviation of the normal distribution used to propose new values in the chain
    sigma4bd = 0.1
    sigma4nukappa = 0.1
    sigma4alphabeta = 0.01

    # chain initialization for spikes
    # we first copy the tree and add the spike feature on all nodes
    t = initTree.copy()
    for n in t.traverse():
        n.add_features(spike = 0)
    # and we propose a first spike configuration with the IS model
    # this could also be any other proposition
    t, loglal = evoltree.eAlChange([alpha, beta, kappa], t, simplifiedAl, columnCounts)
    t = spike.simIS(b, d, nu, t)

    # we now need the probability to be in this state
    t2, llAcondTS = evoltree.loglAl([alpha, beta, kappa], t, simplifiedAl, columnCounts)
    llScondT = spike.loglBS(b, d, nu, t)
    llT = tree.loglBD(b, d, t)
    ll = llAcondTS + llScondT + llT

    # periodicity of recordings
    frec = (niter - nburnin) / nsamp
    
    # We prepare the recording folder and file
    os.chdir(folderName)
    with open("result_params.txt", "a") as fichier:
       fichier.write("logLik" + '\t')
       fichier.write("n" + '\t')
       fichier.write("accept" + '\t')
       fichier.write("lambda" + '\t')
       fichier.write("d" + '\t')
       fichier.write("nu" + '\t')
       fichier.write("kappa" + '\t')
       fichier.write("alpha" + '\t')
       fichier.write("beta" + '\n')

    # Chain body
    for n in range(1,niter+1):
        # we make a new proposition
        # this could be changed to accelerate the convergence
        # as long as it remains coherent with the corresponding
        # likelihood computation
        
        v1 = np.random.random()
        if v1 < 1/8 :
            if verbose:
                print("Proposition: change in lambda, d")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = nu
            propKappa = kappa
            propAlpha = alpha
            propBeta = beta
            propT = t

        elif v1 < 2/8:
            if verbose:
                print("Proposition: change in nu, kappa")
            propD = d
            propBmd = bmd
            propB = b
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa = simNormInX1X2(kappa, sigma4nukappa, 0, 1)
            propAlpha = alpha
            propBeta = beta
            propT = t

        elif v1 < 3/8:
            if verbose:
                print("Proposition: change in alpha, beta")
            propD = d
            propBmd = bmd
            propB = b
            propNu = nu
            propKappa = kappa
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propT = t

        elif v1 < 4/8:
            if verbose:
                print("Proposition: change in lambda, d and nu, kappa")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa = simNormInX1X2(kappa, sigma4nukappa, 0, 1)
            propAlpha = alpha
            propBeta = beta
            propT = t

        elif v1 < 5/8:
            if verbose:
                print("Proposition: change in lambda, d and alpha, beta")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = nu
            propKappa = kappa
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propT = t

        elif v1 < 6/8:
            if verbose:
                print("Proposition: change in nu, kappa and alpha, beta")
            propD = d
            propBmd = bmd
            propB = b
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa = simNormInX1X2(kappa, sigma4nukappa, 0, 1)
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propT = t
 
        elif v1 < 7/8:
            if verbose:
                print("Proposition: change in lambda, d, nu, kappa, alpha, beta")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa = simNormInX1X2(kappa, sigma4nukappa, 0, 1)
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propT = t

        else:
            if verbose:
                print("Proposition: change in spike config")
            propD = d
            propBmd = bmd
            propB = b
            propNu = nu
            propKappa = kappa
            propAlpha = alpha
            propBeta = beta
            propT = spike.simAS2(t)
            
        # we assess the likelihood of the proposition
        t2, llAcondTSProp = evoltree.loglAl([propAlpha, propBeta, propKappa], propT, simplifiedAl, columnCounts)
        llScondTProp = spike.loglBS(propB, propD, propNu, propT)
        llTProp = tree.loglBD(propB, propD, propT)
        llProp = llAcondTSProp + llScondTProp + llTProp

        # we also need the probability of the transition
        if b != propB and nu == propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1)
        elif b == propB and nu != propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa, kappa, sigma4nukappa, 0, 1) 
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa, propKappa, sigma4nukappa, 0, 1)
        elif b == propB and nu == propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1)
        elif b != propB and nu != propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa, kappa, sigma4nukappa, 0, 1) 
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa, propKappa, sigma4nukappa, 0, 1)
        elif b != propB and nu == propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1)
        elif b == propB and nu != propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa, kappa, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa, propKappa, sigma4nukappa, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1)
        elif b != propB and nu != propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa, kappa, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa, propKappa, sigma4nukappa, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1)
        else:
            llTransitionPrev2Prop = log(1/8) + spike.loglAS2(t, propT)
            llTransitionProp2Prev = log(1/8) + spike.loglAS2(propT, t)

        # Metropolis Hasting rule decides if we keep the proposed state or not
        logRatio = llProp - ll + llTransitionProp2Prev - llTransitionPrev2Prop
         
        if logRatio < 0:
            ratio = exp( logRatio )
        else:
            ratio = 1
        
        if verbose:
            print('llProp = ', llProp)
            print('ll = ', ll)
            print('llTransitionProp2Prev = ', llTransitionProp2Prev)
            print('llTransitionPrev2Prop = ', llTransitionPrev2Prop)
            print('ratio = ', ratio)
        
        if np.random.random() < ratio:
            ll = llProp
            d = propD
            bmd = propBmd
            b = propB
            nu = propNu
            kappa = propKappa
            alpha = propAlpha
            beta = propBeta
            t = propT
            accept = 1
            if verbose:
                print('The chain moves\n')
        else:
            accept = 0
            if verbose:
                print('The chain does not move\n')

        # Last, we want to record the chain values from time to time
        if n > nburnin and floor((n-nburnin)/frec) != floor((n+1-nburnin)/frec):
            print("recording step " + str(n) + "/" + str(niter))
            with open("result_params.txt", "a") as fichier:
                fichier.write(str(ll) + '\t')
                fichier.write(str(n) + '\t')
                fichier.write(str(accept) + '\t')
                fichier.write(str(b) + '\t')
                fichier.write(str(d) + '\t')
                fichier.write(str(nu) + '\t')
                fichier.write(str(kappa) + '\t')
                fichier.write(str(alpha) + '\t')
                fichier.write(str(beta) + '\n')
            s2 = ""
            for nd in t.iter_descendants("postorder"):
                s2 += str(nd.spike) + " "
            with open("result_spikes.txt", "a") as fichier:
                fichier.write(s2 + '\n')

    # We get out of the recording folder	
    os.chdir("..")

    # we return these lists to assess visually the convergence of the chain
    return 'Chain completed. You can run a quick diagnostic like graphics.analyzeMcmc(...).'
    

def mcmc3pos(initTree, sAl1, sAl2, sAl3, cCounts1, cCounts2, cCounts3, niter, nburnin, nsamp, folderName, verbose=False):
    '''This Markov Chain samples the posterior distribution
    of spikes along the tree, together with (lambda, d, nu, alpha, beta, u, v, kappa1, kappa2, kappa3)
    posterior values.
    It takes into account the alignment for first, second and third position of codons.
    And considers a model where:
    - the first position evolves with basic rates (alpha, beta) and spike intensity kappa1
    - the second position evolves with basic rates (u*alpha, u*beta) and spike intensity kappa2
    - the third one evolves with basic rates (v*alpha, v*beta) and spike intensity kappa3.
    Note that initialization and moves could be changed to ensure faster convergence.
    Priors could also be changed according to the problem under study.'''
                
    # chain initialization for parameter values
    # possible changes : do whatever you want there.
    d = simNormInX1X2(0.5, 0.001, 0, 1)
    bmd = simNormInX1X2(0.1, 0.001, 0, 1)
    b = d+bmd
    nu = simNormInX1X2(0.015, 0.00001, 0, 1)
    kappa1 = simNormInX1X2(0.035, 0.00001, 0, 1)
    kappa2 = simNormInX1X2(0.025, 0.00001, 0, 1)
    kappa3 = simNormInX1X2(0.043, 0.00001, 0, 1)
    alpha = simNormInX1X2(0.00045, 0.0000001, 0, 0.01)
    beta = simNormInX1X2(0.00022, 0.0000001, 0, 0.01)
    uU = simNormInX1X2(1., 0.0001, 0, 5)
    vV = simNormInX1X2(1., 0.0001, 0, 5)
    
    # standard deviation of the normal distribution used to propose new values in the chain
    sigma4bd = 0.01
    sigma4nukappa = 0.0001
    sigma4alphabeta = 0.00001

    # chain initialization for spikes
    # we first copy the tree and add the spike feature on all nodes
    t = initTree.copy()
    for n in t.traverse():
        n.add_features(spike = 0)
    # and we propose a first spike configuration with the IS model
    # this could also be any other proposition
    t, loglal3 = evoltree.eAlChange([vV*alpha, vV*beta, kappa3], t, sAl3, cCounts3)
    t = spike.simIS(b, d, nu, t)

    # we now need the probability to be in this state
    t2, llA1condTS = evoltree.loglAl([alpha, beta, kappa1], t, sAl1, cCounts1)
    t2, llA2condTS = evoltree.loglAl([uU*alpha, uU*beta, kappa2], t, sAl2, cCounts2)
    t2, llA3condTS = evoltree.loglAl([vV*alpha, vV*beta, kappa3], t, sAl3, cCounts3)
    llScondT = spike.loglBS(b, d, nu, t)
    llT = tree.loglBD(b, d, t)
    ll = llA1condTS + llA2condTS + llA3condTS + llScondT + llT

    # periodicity of recordings
    frec = (niter - nburnin) / nsamp
    
    # We prepare the recording folder and file
    os.chdir(folderName)
    with open("result_params.txt", "a") as fichier:
       fichier.write("logLik" + '\t')
       fichier.write("n" + '\t')
       fichier.write("accept" + '\t')
       fichier.write("lambda" + '\t')
       fichier.write("d" + '\t')
       fichier.write("nu" + '\t')
       fichier.write("kappa1" + '\t')
       fichier.write("kappa2" + '\t')
       fichier.write("kappa3" + '\t')
       fichier.write("alpha" + '\t')
       fichier.write("beta" + '\t')
       fichier.write("u" + '\t')
       fichier.write("v" + '\n')

    # Chain body
    for n in range(1,niter+1):
        # we make a new proposition
        # this could be changed to accelerate the convergence
        # as long as it remains coherent with the corresponding
        # likelihood computation
        
        v1 = np.random.random()
        if v1 < 1/8 :
            if verbose:
                print("Proposition: change in lambda, d")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = nu
            propKappa1 = kappa1
            propKappa2 = kappa2
            propKappa3 = kappa3
            propAlpha = alpha
            propBeta = beta
            propU = uU
            propV = vV
            propT = t

        elif v1 < 2/8:
            if verbose:
                print("Proposition: change in nu, kappa1, kappa2, kappa3")
            propD = d
            propBmd = bmd
            propB = b
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa1 = simNormInX1X2(kappa1, sigma4nukappa, 0, 1)
            propKappa2 = simNormInX1X2(kappa2, sigma4nukappa, 0, 1)
            propKappa3 = simNormInX1X2(kappa3, sigma4nukappa, 0, 1)
            propAlpha = alpha
            propBeta = beta
            propU = uU
            propV = vV
            propT = t

        elif v1 < 3/8:
            if verbose:
                print("Proposition: change in alpha, beta, u, v")
            propD = d
            propBmd = bmd
            propB = b
            propNu = nu
            propKappa1 = kappa1
            propKappa2 = kappa2
            propKappa3 = kappa3
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propU = simNormInX1X2(uU, sigma4alphabeta, 0, 5)
            propV = simNormInX1X2(vV, sigma4alphabeta, 0, 5)
            propT = t

        elif v1 < 4/8:
            if verbose:
                print("Proposition: change in lambda, d and nu, kappa1, kappa2, kappa3")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa1 = simNormInX1X2(kappa1, sigma4nukappa, 0, 1)
            propKappa2 = simNormInX1X2(kappa2, sigma4nukappa, 0, 1)
            propKappa3 = simNormInX1X2(kappa3, sigma4nukappa, 0, 1)
            propAlpha = alpha
            propBeta = beta
            propU = uU
            propV = vV
            propT = t

        elif v1 < 5/8:
            if verbose:
                print("Proposition: change in lambda, d and alpha, beta, u, v")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = nu
            propKappa1 = kappa1
            propKappa2 = kappa2
            propKappa3 = kappa3
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propU = simNormInX1X2(uU, sigma4alphabeta, 0, 5)
            propV = simNormInX1X2(vV, sigma4alphabeta, 0, 5)
            propT = t

        elif v1 < 6/8:
            if verbose:
                print("Proposition: change in nu, kappa1, kappa2, kappa3, and alpha, beta, u, v")
            propD = d
            propBmd = bmd
            propB = b
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa1 = simNormInX1X2(kappa1, sigma4nukappa, 0, 1)
            propKappa2 = simNormInX1X2(kappa2, sigma4nukappa, 0, 1)
            propKappa3 = simNormInX1X2(kappa3, sigma4nukappa, 0, 1)
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propU = simNormInX1X2(uU, sigma4alphabeta, 0, 5)
            propV = simNormInX1X2(vV, sigma4alphabeta, 0, 5)
            propT = t
 
        elif v1 < 7/8:
            if verbose:
                print("Proposition: change in lambda, d, nu, kappa1, kappa2, kappa3, alpha, beta, u, v")
            propD = simNormInX1X2(d, sigma4bd, 0, 10)
            propBmd = simNormInX1X2(bmd, sigma4bd, 0, 1)
            propB = propD+propBmd
            propNu = simNormInX1X2(nu, sigma4nukappa, 0, 1)
            propKappa1 = simNormInX1X2(kappa1, sigma4nukappa, 0, 1)
            propKappa2 = simNormInX1X2(kappa2, sigma4nukappa, 0, 1)
            propKappa3 = simNormInX1X2(kappa3, sigma4nukappa, 0, 1)
            propAlpha = simNormInX1X2(alpha, sigma4alphabeta, 0, 0.1)
            propBeta = simNormInX1X2(beta, sigma4alphabeta, 0, 0.1)
            propU = simNormInX1X2(uU, sigma4alphabeta, 0, 5)
            propV = simNormInX1X2(vV, sigma4alphabeta, 0, 5)
            propT = t

        else:
            if verbose:
                print("Proposition: change in spike config")
            propD = d
            propBmd = bmd
            propB = b
            propNu = nu
            propKappa1 = kappa1
            propKappa2 = kappa2
            propKappa3 = kappa3
            propAlpha = alpha
            propBeta = beta
            propU = uU
            propV = vV
            propT = spike.simAS2(t)
            
        # we assess the likelihood of the proposition
        t2, llA1condTSProp = evoltree.loglAl([propAlpha, propBeta, propKappa1], propT, sAl1, cCounts1)
        t2, llA2condTSProp = evoltree.loglAl([propU*propAlpha, propU*propBeta, propKappa2], propT, sAl2, cCounts2)
        t2, llA3condTSProp = evoltree.loglAl([propV*propAlpha, propV*propBeta, propKappa3], propT, sAl3, cCounts3)
        llScondTProp = spike.loglBS(propB, propD, propNu, propT)
        llTProp = tree.loglBD(propB, propD, propT)
        llProp =  llA1condTSProp + llA2condTSProp + llA3condTSProp + llScondTProp + llTProp

        # we also need the probability of the transition
        if b != propB and nu == propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1)
        elif b == propB and nu != propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa1, kappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa2, kappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa3, kappa3, sigma4nukappa, 0, 1) 
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1)  + logPdfNormInX1X2(kappa1, propKappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa2, propKappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa3, propKappa3, sigma4nukappa, 0, 1)
        elif b == propB and nu == propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propU, uU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(propV, vV, sigma4alphabeta, 0, 5)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(uU, propU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(vV, propV, sigma4alphabeta, 0, 5)
        elif b != propB and nu != propNu and alpha == propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa1, kappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa2, kappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa3, kappa3, sigma4nukappa, 0, 1) 
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa1, propKappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa2, propKappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa3, propKappa3, sigma4nukappa, 0, 1)
        elif b != propB and nu == propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propU, uU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(propV, vV, sigma4alphabeta, 0, 5)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(uU, propU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(vV, propV, sigma4alphabeta, 0, 5)
        elif b == propB and nu != propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa1, kappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa2, kappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa3, kappa3, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propU, uU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(propV, vV, sigma4alphabeta, 0, 5)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa1, propKappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa2, propKappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa3, propKappa3, sigma4nukappa, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(uU, propU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(vV, propV, sigma4alphabeta, 0, 5)
        elif b != propB and nu != propNu and alpha != propAlpha:
            llTransitionPrev2Prop = log(1/8) + logPdfNormInX1X2(propD, d, sigma4bd, 0, 10) + logPdfNormInX1X2(propBmd, bmd, sigma4bd, 0, 1) + logPdfNormInX1X2(propNu, nu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa1, kappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa2, kappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propKappa3, kappa3, sigma4nukappa, 0, 1) + logPdfNormInX1X2(propAlpha, alpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propBeta, beta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(propU, uU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(propV, vV, sigma4alphabeta, 0, 5)
            llTransitionProp2Prev = log(1/8) + logPdfNormInX1X2(d, propD, sigma4bd, 0, 10) + logPdfNormInX1X2(bmd, propBmd, sigma4bd, 0, 1) + logPdfNormInX1X2(nu, propNu, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa1, propKappa1, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa2, propKappa2, sigma4nukappa, 0, 1) + logPdfNormInX1X2(kappa3, propKappa3, sigma4nukappa, 0, 1) + logPdfNormInX1X2(alpha, propAlpha, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(beta, propBeta, sigma4alphabeta, 0, 0.1) + logPdfNormInX1X2(uU, propU, sigma4alphabeta, 0, 5) + logPdfNormInX1X2(vV, propV, sigma4alphabeta, 0, 5)
        else:
            llTransitionPrev2Prop = log(1/8) + spike.loglAS2(t, propT)
            llTransitionProp2Prev = log(1/8) + spike.loglAS2(propT, t)

        # Metropolis Hasting rule decides if we keep the proposed state or not
        logRatio = llProp - ll + llTransitionProp2Prev - llTransitionPrev2Prop
         
        if logRatio < 0:
            ratio = exp( logRatio )
        else:
            ratio = 1
        
        if verbose:
            print('llProp = ', llProp)
            print('ll = ', ll)
            print('llTransitionProp2Prev = ', llTransitionProp2Prev)
            print('llTransitionPrev2Prop = ', llTransitionPrev2Prop)
            print('ratio = ', ratio)
        
        if np.random.random() < ratio:
            ll = llProp
            d = propD
            bmd = propBmd
            b = propB
            nu = propNu
            kappa1 = propKappa1
            kappa2 = propKappa2
            kappa3 = propKappa3
            alpha = propAlpha
            beta = propBeta
            uU = propU
            vV = propV
            t = propT
            accept = 1
            if verbose:
                print('The chain moves\n')
        else:
            accept = 0
            if verbose:
                print('The chain does not move\n')

        # Last, we want to record the chain values from time to time
        if n > nburnin and floor((n-nburnin)/frec) != floor((n+1-nburnin)/frec):
            print("recording step " + str(n) + "/" + str(niter))
            with open("result_params.txt", "a") as fichier:
                fichier.write(str(ll) + '\t')
                fichier.write(str(n) + '\t')
                fichier.write(str(accept) + '\t')
                fichier.write(str(b) + '\t')
                fichier.write(str(d) + '\t')
                fichier.write(str(nu) + '\t')
                fichier.write(str(kappa1) + '\t')
                fichier.write(str(kappa2) + '\t')
                fichier.write(str(kappa3) + '\t')
                fichier.write(str(alpha) + '\t')
                fichier.write(str(beta) + '\t')
                fichier.write(str(uU) + '\t')
                fichier.write(str(vV) + '\n')
            s2 = ""
            for nd in t.iter_descendants("postorder"):
                s2 += str(nd.spike) + " "
            with open("result_spikes.txt", "a") as fichier:
                fichier.write(s2 + '\n')

    # We get out of the recording folder	
    os.chdir("..")

    # we return these lists to assess visually the convergence of the chain
    return 'Chain completed. You can run a quick diagnostic like graphics.analyzeMcmc(...).'
 
