from ete3 import Tree
import numpy as np
import matplotlib.pyplot as plt
from math import log, exp

def simFullTree(b, d, to, tf):
    ''' Simulation of a naive b-d tree with all branches, extinct or not. '''
    B = np.random.exponential( 1 / b )
    D = np.random.exponential( 1 / d )
    if B > tf-to and D > tf-to:
        newick = "f:" + str(tf-to)
    elif D > B:
        left_sister = simFullTree(b, d, to+B, tf)
        right_sister = simFullTree(b, d, to+B, tf)
        newick = "(" + left_sister + "," + right_sister + "):" + str(B)
    else:
        newick = "f:" + str(D) 

    return newick


def annotateExtinctNodes(t, to, tf):
    '''Adds a boolean label "extant" which is True if the subtree goes to the present
    or False if it goes extinct before present. '''
    # we first annotate with the living time of each node
    for n in t.traverse('preorder'):
        if n.is_root():
            n.add_features(time = to + n.dist)
        else:
            n.add_features(time = n.dist + n.up.time)

    # then we turn to the extinct/extant status
    for n in t.traverse("postorder"):
        if n.is_leaf():
            n.add_features(extant = (n.time == tf) )
        else:
            n.add_features(extant = (n.children[0].extant or n.children[1].extant) )

    return t


def getExtinctBranching(t):
    ''' Counts the number of branching events with an extinct subtree along one extant lineage.
    This function must be applied on an annotated tree, with the root being "extant == True" '''
    if t.is_leaf():
        nb = 0
    else:
        if t.children[0].extant and t.children[1].extant:
            nb = getExtinctBranching(t.children[0])
        elif t.children[0].extant and not(t.children[1].extant):
            nb = 1 + getExtinctBranching(t.children[0])
        elif t.children[1].extant and not(t.children[0].extant):
            nb = 1 + getExtinctBranching(t.children[1])
    return nb


def getSampleExtinctBranchingNb(b, d, to, tf, N):
    ''' We repeat N times the experiment and record the number of extinct branching events along
    one lineage in the tree. '''
    i = 1
    sample = []
    while i < N:
        newick = simFullTree(b, d, to, tf) + ";"
        t = Tree(newick)
        t = annotateExtinctNodes(t, to, tf)
        if t.extant:
            n = getExtinctBranching(t)
            sample.append(n)
            i += 1

    return sample
        

def meanPoisson(b, d, f, to, tf):
    ''' This is the theoretical mean of the Poisson distribution for the number of
    extinct branching events along one lineage in the tree. '''
    numerator = 1. - b*f/(d-b+b*f) * exp((b-d) * tf)
    denominator = 1. - b*f/(d-b+b*f) * exp((b-d) * to)
    return 2. * b * (tf - to) - 2. * log(numerator/denominator)


N = 10000
list_d = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
b, to, tf = 1., 0., 5.
list_empiricalmean, list_theoreticalexpectation = [], []
for d in list_d:
    sample = getSampleExtinctBranchingNb(b, d, to, tf, N)
    list_empiricalmean.append( np.mean(sample) )
    list_theoreticalexpectation.append( meanPoisson(b, d, 1., to, tf) )

plt.plot(list_d, list_theoreticalexpectation, label="theoretical expectation")
plt.plot(list_d, list_empiricalmean, 'o--', label="empirical mean")
plt.legend()
plt.xlabel("death rate d")
plt.ylabel("mean number of branching events with an extinct clade")
plt.show()


# Uncomment what is below if you wish to compare the two histograms of Poisson distributed variables
#b, d, to, tf = 1., 0.5, 0., 5.
#N = 10000
#sample = getSampleExtinctBranchingNb(b, d, to, tf, N)
#plt.subplot(121)
#plt.hist(sample, density=True)
#
#plt.subplot(122)
#meanth = meanPoisson(b, d, 1., to, tf)
#sampleth = np.random.poisson(meanth, N)
#plt.hist(sampleth, density=True)
#plt.show()
#
#print("Mean over simulations: ", np.mean(sample), " matches the theoretical expectation: ", meanth)
