from ete3 import Tree
import numpy as np
from math import log, exp

# This file contains material for simulating BD trees

#    Note that parameter lambda is often called "b" in this file, because
#    this name changed late in the writing process.

def inverseFH(b, d, u):
    ''' Inverse of the repartition function of the distribution of
    node depths, used for cpp-style simulations of a birth-death process. '''
    return log( (b - d*u) / (b - b*u) ) / (b-d)

def simCppBD(b, d, tmax):
    ''' Simulates the node heights of a tree following a birth-death process
    with parameters b, d, starting at time tmax. '''
    cpp = []
    tbranch = tmax
    while tbranch <= tmax:
        cpp.append(tbranch)
        tbranch = inverseFH(b, d, np.random.random() )
    return cpp

def cppToRec(cpp):
    ''' Translates the cpp representation, with successive node-depths, into a
    recursive structure using the ete3 python module. '''
    listOfNodes = []
    for i in range(0, len(cpp)):
        newick = "f" + str(i) + ":0;"
        listOfNodes.append( Tree(newick) )
    
    while len(cpp) > 1:
        # we look for the shortest branch, and we branch it to the left.
        minval, argmin = min( [(cpp[i], i) for i in range(0, len(cpp))] )
        h = cpp.pop(argmin)

        # adding a branch length to the right node
        rightbranch = listOfNodes.pop(argmin)
        farthestnode, dist = rightbranch.get_farthest_node()
        rightbranch.dist = h - dist

        # adding a branch length to the left node
        leftbranch = listOfNodes[argmin-1]
        farthestnode, dist = leftbranch.get_farthest_node()
        leftbranch.dist = h - dist

        # new node on which we branch the two previous ones
        newnode = Tree("n:0;")
        newnode.add_child(leftbranch)
        newnode.add_child(rightbranch)
        listOfNodes[argmin-1] = newnode
    # the last node in the list is the complete tree
    return listOfNodes[0]

def recToCpp(t):
    ''' Translates the recursive representation using the ete3 python module
    into a cpp reprensation with successive node-depths. '''
    cpp = []
    prev = t
    for l in t.iter_leaves():
        if prev == t:
            cpp.append( l.get_distance(t) + t.dist )
        else:
            cpp.append( l.get_distance( t.get_common_ancestor(l, prev) ) )
        prev = l
    return cpp

def simBDtree(b, d, tmax):
    ''' Wrapping together the simulation in CPP-mode, 
    and the conversion into recursive structure. '''
    cpp = simCppBD(b, d, tmax)
    return cppToRec( cpp )

def p1(b, d, rho, t):
    numerator = rho *(b-d)**2 * exp( -(b - d)*t )
    denominator = (rho * b + (b*(1 - rho) - d) * exp( -(b - d)*t ))**2
    result = numerator / denominator
    return result

def p0(b, d, rho, t):
    numerator = rho*(b - d)
    denominator = rho * b + (b*(1 - rho) - d)*exp( -(b-d)*t )
    result = 1 - numerator/denominator
    return result

def llBDCpp(cpp, b, d, rho):
    ''' The exact computation of the likelihood of
    the b,d parameters on an observed cpp-formated
    tree '''
    cpp2 = cpp.copy()
    cpp2.sort()
    n = len(cpp2)
    ll = 0
    
    for i in range(0, n):
        ll += log( p1( b, d, rho, cpp2[i] ) ) 

    ll += (n - 1) * log(b)
    ll -= log( 1 - p0(b, d, rho, cpp2[n-1]) )

    return ll

def loglBD_v0(b, d, rho, t):
    ''' Computes the log likelihood of a BD-tree with
    constant parameters. '''
    return llBDCpp( recToCpp(t), b, d, rho )

def funcQ(b, d, f, time):
    ''' The function called q(t) in Stadler_2010, used to compute
    the likelihood of a BD-tree. '''
    c1 = b-d
    c2 = - (b-d-2*b*f) / c1
    return 2*(1-c2**2) + exp(-c1*time)*((1-c2)**2) + exp(c1*time)*((1+c2)**2)

def loglBD(b, d, t):
    ''' Computes the log-likelihood of a BD-tree with
    constant parameters b and d throughout the tree.
    There is also the possibility to have sampling fractions
    variables throughout the tree t, in which case they must
    be specified as a 'sampf' feature at each nodes.'''

    # recall that time is 0 at the leaves and increases in the past, the tree is ultrametric.
    for n in t.traverse("postorder"):
        # the branch is extending from s1 (near the tips) to t (in the past)
        if n.is_leaf():
            s1 = 0
            time = s1 + n.dist
            logl = log( 4*n.sampf ) - log( funcQ(b, d, n.sampf, time) )
            n.add_features(loglbd = logl)
        else:
            s1 = time
            time = s1 + n.dist
            loglbd0 = n.children[0].loglbd
            loglbd1 = n.children[1].loglbd
            logl = log(b) + loglbd0 + loglbd1 + log( funcQ(b, d, n.sampf, s1) ) - log( funcQ(b, d, n.sampf, time) )
            n.add_features(loglbd = logl)

    return t.loglbd

