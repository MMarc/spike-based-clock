import tree
import nt
import numpy
import os
import mcmc
import export
import graphics

newick = "(((((((((((((Pseudechis_porphyriacus:18.6,Pseudechis_australis:18.6)Node14:0,Oxyuranus_scutellatus:18.6)Node13:9,(Notechis_scutatus:9.58011,Austrelaps_superbus:9.58011)Node15:18.01989)Node12:0,((Drysdalia_coronoides:16.81360667,Tropidechis_carinatus:16.81360667)Node17:3.58639333,Demansia_vestigiata:20.4)Node16:7.2)Node11:5.110056,Bungarus_candidus:32.710056)Node10:2.289944,((Naja_kaouthia:13.61493,Naja_atra:13.61493)Node19:21.38507,Ophiophagus_hannah:35)Node18:0)Node9:0,Hydrophis_elegans:35)Node8:11.4,Leioheterodon_madagascariensis:46.4)Node7:4.993106,(((Trimorphodon_biscutatus:37.078045,Dispholidus_typus:37.078045)Node22:10.521955,Thamnophis_sirtalis:47.6)Node21:0,(Philodryas_olfersii:33.41441,Erythrolamprus_poecilogyrus:33.41441)Node23:14.18559)Node20:3.793106)Node6:2.641354,(Cerberus_rynchops:21.659915,Pseudoferania_polylepis:21.659915)Node24:32.374545)Node5:7.70362091,((((((((((((Crotalus_adamanteus:6.6268525,Crotalus_oreganus:6.6268525)Node36:1.3063815,Crotalus_atrox:7.933234)Node35:2.7878885,Crotalus_horridus:10.7211225)Node34:1.76022528,Sistrurus_catenatus:12.48134778)Node33:4.48308333,Agkistrodon_piscivorus:16.96443111)Node32:1.83012889,Gloydius_blomhoffii:18.79456)Node31:4.71982286,Cerrophidion_godmani:23.51438286)Node30:5.00283714,(Trimeresurus_gracilis:27.42444,Trimeresurus_stejnegeri:27.42444)Node37:1.09278)Node29:0.9973575,((Protobothrops_flavoviridis:11.4324,Protobothrops_jerdonii:11.4324)Node39:15.634,Bothriechis_schlegelii:27.0664)Node38:2.4481775)Node28:9.2854225,Deinagkistrodon_acutus:38.8)Node27:0,Azemiops_feae:38.8)Node26:3.44041231,(((Vipera_nikolskii:1.794205,Vipera_berus:1.794205)Node42:15.68761,Daboia_russelii:17.481815)Node41:18.38356333,Atheris_nitschei:35.86537833)Node40:6.37503397)Node25:19.4976686)Node4:29.06191909,Python_sebae:90.8)Node3:76.32419,((((((Varanus_gouldii:9.512345,Varanus_panoptes:9.512345)Node48:8.499115,Varanus_mertensi:18.01146)Node47:7.23287,Varanus_komodoensis:25.24433)Node46:7.34712,((Varanus_acanthurus:21.238715,Varanus_gilleni:21.238715)Node50:3.96576,Varanus_scalaris:25.204475)Node49:7.386975)Node45:98.54301727,((Gerrhonotus_infernalis:61.5,Celestus_haetianus:61.5)Node52:43.05353353,(Heloderma_suspectum:21.69252333,Heloderma_horridum:21.69252333)Node53:82.8610102)Node51:26.58093374)Node44:34.09463503,Anolis_carolinensis:165.2291023)Node43:1.89508804)Node1;"

t = tree.Tree(newick, format=1)

# We fix a basic f = 0.01
for n in t.traverse():
    n.add_features(sampf = 0.01)
    
# And subclade subtended by Node52, as well as two specific long leaves, with f = 0.02
node = t.search_nodes(name="Node52")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.023)
node = t.search_nodes(name="Anolis_carolinensis")[0]
node.add_features(sampf = 0.022)
node = t.search_nodes(name="Python_sebae")[0]
node.add_features(sampf = 0.024)    

# And subclade subtended by Node8 with f = 0.034
node = t.search_nodes(name="Node8")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.034)

# And subclade subtended by Node24 with f = 0.036
node = t.search_nodes(name="Node24")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.036)
    
# And subclade subtended by Node20, as well as two specific long leaves, with f = 0.002
node = t.search_nodes(name="Node20")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.002)
node = t.search_nodes(name="Leioheterodon_madagascariensis")[0]
node.add_features(sampf = 0.003)

# And subclade subtended by Node25 with f = 0.05
node = t.search_nodes(name="Node25")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.05)

# And subclade subtended by Node45 with f = 0.86
node = t.search_nodes(name="Node45")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 0.086)

# And subclade subtended by Node53 with f = 1
node = t.search_nodes(name="Node53")[0]
for n in node.iter_descendants():
    n.add_features(sampf = 1)

# Check visually that it works as expected
t.show()

# Order of taxa as given by the tree leaves
names = []
for n in t.iter_leaves():
    names.append( n.name )

# Reading the sequence and making the alignment in the correct order
k = 1
Al1, Al2, Al3 = [], [], []
names2 = []
file = open("CRISP_ordered_cut1.fasta", "r")
for line in file.readlines():
    if k % 2 == 0 :
        seq1, seq2, seq3 = [], [], []
        for j in range(0, len(line)-1):
            if j % 3 == 0:
                seq1.append( nt.strToNt( line[j] ) )
            elif (j+2) % 3 == 0:
                seq2.append( nt.strToNt( line[j] ) )
            elif (j+1) % 3 == 0:
                seq3.append( nt.strToNt( line[j] ) )
        Al1.append(seq1)
        Al2.append(seq2)
        Al3.append(seq3)
    else:
        names2.append( line[1:len(line)-1] )
    k += 1

# check that the names are in the same order, meaning each sequence is assigned the right taxon
print('Please double check that the following taxa are the same, to ensure that each leaf of the tree is assigned the correct sequence :')
for i in range(0, len(names)):
    print(names[i], ' ---> ', names2[i])
al1 = numpy.asarray(Al1)
al2 = numpy.asarray(Al2)
al3 = numpy.asarray(Al3)
sAl1, cCounts1 = nt.simplifyAlignment(al1, nt.cmpColumnsBasic)
sAl2, cCounts2 = nt.simplifyAlignment(al2, nt.cmpColumnsBasic)
sAl3, cCounts3 = nt.simplifyAlignment(al3, nt.cmpColumnsBasic)

# All outputs of this example will be stored in
folderName = 'InferenceOnCRISP3pos'
os.mkdir(folderName)

# run the mcmc
niter, nburnin, nsamp = 1000000, 0, 50000
mcmc.mcmc3pos(t, sAl1, sAl2, sAl3, cCounts1, cCounts2, cCounts3, niter, nburnin, nsamp, folderName)

# we plot the histograms of parameter inferences
graphics.analyzeMcmc3pos(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, t, folderName)

# export Julie-style
t4 = export.meanSpike(folderName, "result_spikes.txt", t, 0)

