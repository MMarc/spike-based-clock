import numpy as np
from ete3 import Tree
from math import exp, log, factorial

# This file contains algorithms for simulating spikes along trees
# under various models, as well as the corresponding functions
# to compute the likelihood of a spiked tree under various models.

#    Note that parameter lambda is often called "b" in this file, because
#    this name changed late in the writing process. However, all outputs
#    should be consistent with "lambda".

def meanNS(b, d, nu, f, t0, t1):
    '''Mean number of spikes under the 'Branching with Spikes' model,
    on a branch extending from t0 to t1, with model parameters
    b, d, nu.'''
    numerator = 1. - b*f/(d-b+b*f) * exp((b-d) * t0)
    denominator = 1. - b*f/(d-b+b*f) * exp((b-d) * t1)
    return 2. * b * nu * (t0 - t1) - 2. * nu * log(numerator/denominator)


def pPoisson(alpha, n):
    ''' Probability mass function of a Poisson random variable with paramater alpha.'''
    if n < 0:
        result = "Error: the number of spikes should be a non-negative integer."
    else:
        result = exp( -alpha ) * (alpha**n) / (factorial(n))
    return result

def pPoissonBernoulli(alpha, nu, n):
    ''' Probability mass function of a random variable that is the sum of a Poisson(alpha)
    and a Bernoulli(nu). '''
    if n == 0:
        result = (1. - nu) * exp(-alpha)
    else:
        pnminus1 = pPoisson(alpha, n-1)
        pn = pnminus1 * alpha / float(n)
        result = nu*pnminus1 + (1-nu)*pn
    return result

def logistic(a, x):
    ''' Logistic function with parameter a. '''
    if x > -709.7:
        result = 1. / (1. + a * exp(-x))
    else:
        result = 0
    return result

def simBS(b, d, nu, t):
    ''' Simulates spikes along a tree t, under the 'Branching with Spikes' model
    with parameters b, d, nu.
    The sampling fraction f must be recorded as a feature on each node of the tree.'''
    t2 = t.copy()

    # we traverse the tree in any order. 
    # recall that time is 0 at the leaves and increases in the past, the tree is ultrametric.
    time = 0
    for n in t2.iter_descendants("postorder"):
        # the branch is extending from t1 (near the tips) to t0 (in the past)
        if n.is_leaf():
            t1 = 0
        else:
            t1 = time
        t0 = t1 + n.dist
        time = t0

        # we simulate a Poisson(alpha) or Poisson(alpha) + Bernouilli(nu) random number of spikes.
        alpha = meanNS(b, d, nu, n.sampf, t0, t1)
        if n.is_leaf():
            nS = np.random.poisson(alpha)
            n.add_features(spike = nS)
        else:
            nS = np.random.poisson(alpha) + np.random.binomial(1,nu)
            n.add_features(spike = nS)

    # last, we add 0 spike to the root
    t2.add_features(spike = 0)
    return t2

def loglBS(b, d, nu, t):
    ''' Computes the probability mass function of the number of spikes on a tree t,
    under the 'Branching with Spikes' model with parameters b, d, nu.
    Note that the tree t must have a feature called 'spike' displaying the number of spikes.
    The sampling fraction f must be recorded as a feature on each node of the tree.'''
    logl = 0
    time = 0
    for n in t.iter_descendants("postorder"):
        # the branch is extending from t1 (near the tips) to t0 (in the past)
        if n.is_leaf():
            t1 = 0
        else:
            t1 = time
        t0 = t1 + n.dist
        time = t0

        alpha = meanNS(b, d, nu, n.sampf, t0, t1)
        if n.is_leaf():
            prob = pPoisson(alpha, n.spike) 
            if prob <= 0:
                logl += -1e500
            else:
                logl += log( prob )
        else:
            prob = pPoissonBernoulli(alpha, nu, n.spike)
            if prob <= 0:
                logl += -1e500
            else:
                logl += log( prob )
    return logl

def simIS(b, d, nu, t):
    ''' Simulates spikes according to the model attaching preferentially spikes where
    the feature called "value" is high enough.
    Note that this requires each node to have a "value" feature and a "spike" feature.
    The sampling fraction f must be recorded as a feature on each node of the tree.'''
    t2 = t.copy()
    
    # we traverse the tree in any order. 
    # recall that time is 0 at the leaves and increases in the past, the tree is ultrametric.
    time = 0
    for n in t2.iter_descendants("postorder"):
        # the branch is extending from t1 (near the tips) to t0 (in the past)
        if n.is_leaf():
            t1 = 0
        else:
            t1 = time
        t0 = t1 + n.dist
        time = t0

        # we simulate a Poisson(alpha) + Bernouilli(logistic(value)) random number of spikes.
        alpha = meanNS(b, d, nu, n.sampf, t0, t1)
        p = logistic((1-nu)/nu * exp(1.96), n.value)
        n.spike = np.random.poisson(alpha) + np.random.binomial(1,p)
    
    return t2

def loglIS(b, d, nu, t):
    ''' Computes the log of the probability mass function of spike numbers along tree t,
    according to the model attaching preferentially spikes where
    the feature called "value" is high enough.
    Note that this requires each node to have a "value" feature and a "spike" feature.
    The sampling fraction f must be recorded as a feature on each node of the tree.'''
    logl = 0
    # we traverse the tree in any order. 
    # recall that time is 0 at the leaves and increases in the past, the tree is ultrametric.
    time = 0
    for n in t.iter_descendants("postorder"):
        # the branch is extending from t1 (near the tips) to t0 (in the past)
        if n.is_leaf():
            t1 = 0
        else:
            t1 = time
        t0 = t1 + n.dist
        time = t0

        alpha = meanNS(b, d, nu, n.sampf, t0, t1)
        p = logistic((1-nu)/nu * exp(1.96), n.value)
        logl += log( pPoissonBernoulli(alpha, p, n.spike) )
    return logl

def simPS(nu, t):
    ''' Simulates spikes on tree t according to the "Proposal for Spike" model,
    used as part of the MCMC to move spikes along branches.
    It must be applied to a tree t displaying "value" and "spike" features. '''
    t2 = t.copy()
    
    for n in t2.iter_descendants("postorder"):
        c = (1 - nu) / nu * exp(4)
        probamoins = logistic(c, -n.value)
        probaplus = logistic(c, n.value)

        # adding or removing a spike on the current branch
        if np.random.random() < probaplus:
            n.spike += 1
        if n.spike > 0 and np.random.random() < probamoins:
            n.spike -= 1
    
    return t2

def loglPS(nu, t1, t2):
    ''' Computes the log probability to get the tree t2 from tree t1,
    under the "PS" model, used for proposal in the MCMC.
    It must be applied to two trees t1 and t2 displaying features
    "spike" and "value", and having same topologies and branch-lengths. '''
    logl = 0
    # this iterates simultaneously over the nodes of the two trees
    for (n1, n2) in zip(t1.iter_descendants("postorder"), t2.iter_descendants("postorder")):
        c = (1 - nu) / nu * exp(1)
        probamoins = logistic(c, -n1.value)
        probaplus = logistic(c, n1.value)

        # different scenarios and their probabilities
        if n2.spike == n1.spike + 1:
            proba = probaplus * (1 - probamoins)
        elif n2.spike == n1.spike - 1:
            proba = probamoins * (1 - probaplus)
        elif n2.spike == 0 and n1.spike == 0:
            proba = 1 - probaplus + probaplus * probamoins
        else:
            proba = probaplus * probamoins + (1 - probaplus)*(1 - probamoins)

        if proba > 0:
            logl += log( proba )

    return logl


def simAS(t):
    ''' Simulates spikes along a tree t already displaying a 'spike' feature.
    With probability 0.5, it removes a previously displayed spike, on a branch chosen
    uniformly among all branches having at least one spike (if there is no
    spike, it does not change anything).
    With probability 0.5, it adds a spike on a branch chosen uniformly among all branches. '''
    t2 = t.copy()

    if np.random.random() < 0.5:
        # we count the number of nodes having at least one spike
        nodeNb = len([n for n in t2.traverse() if n.spike > 0])
        # we chose one uniformly
        if nodeNb > 0:
            chosenOne = np.random.randint(0, nodeNb)
            # and we remove one of its spikes
            number = 0
            for n in t2.traverse():
                if n.spike > 0:
                    if number == chosenOne:
                        n.spike -= 1
                    number += 1

    else:
        # we count the number of nodes
        nodeNb = len([n for n in t2.traverse()])
        # we chose one uniformly
        chosenOne = np.random.randint(0, nodeNb)
        # on which we add a spike
        number = 0
        for n in t2.traverse():
            if number == chosenOne:
                n.spike += 1
            number += 1

    return t2

def loglAS(t1, t2):
    ''' Computes the log probability to go from tree t1 to tree t2 under the AS model.
    The two trees must have the exact same topology and must display a 'spike' feature at all nodes.'''
    # counts the nb of spikes added, 
    # the nb of spikes substracted between the two trees, 
    # as well as the spike number in the first tree
    nplus, nmoins, spikeNb = 0, 0, 0
    for (n1, n2) in zip(t1.traverse(), t2.traverse()):
        spikeNb += n1.spike
        if n1.spike > n2.spike:
            nmoins += n1.spike - n2.spike
        elif n2.spike > n1.spike:
            nplus += n2.spike - n1.spike

    # the log probability corresponding to the distinct events
    if nplus == 0 and nmoins == 1:
        result = np.log(0.5)
    elif nmoins == 0 and nplus == 1:
        result = np.log(0.5)
    elif nmoins == 0 and nplus == 0 and spikeNb == 0:
        result = np.log(0.5)
    else:
        result = -np.inf

    return result

def simAS2(t):
    ''' Simulates spikes along a tree t already displaying a 'spike' feature.
    With probability 0.5, it removes a previously displayed spike, on a branch chosen
    uniformly among all branches having at least one spike (if there is no
    spike, it does not change anything).
    With probability 0.5, it adds a spike on a branch chosen uniformly among all branches. '''
    t2 = t.copy()
    # we count the number of nodes having at least one spike
    spikedNodeNb = len([n for n in t2.traverse() if n.spike > 0])
    nodeNb = len([n for n in t2.traverse()])

    if spikedNodeNb > 0 and np.random.random() < 0.05:
        # we choose a number of nodes that we want to delete
        delNb = np.random.randint(1, spikedNodeNb+1)
        # we then choose their indices
        chosenOnes = np.random.choice(spikedNodeNb, delNb, replace=False)
        # and we remove them
        number = 0
        for n in t2.traverse():
            if n.spike > 0:
                if number in chosenOnes:
                    n.spike -= 1
                number += 1

    else:
        # we choose a number of nodes that we want to add
        addNb = 1 + np.random.poisson(1)
        # we then choose their indices
        chosenOnes = np.random.choice(nodeNb, addNb, replace=False)
        # and we add spikes accordingly
        number = 0
        for n in t2.traverse():
            if number in chosenOnes:
                n.spike += 1
            number += 1

    return t2

def loglAS2(t1, t2):
    ''' Computes the log probability to go from tree t1 to tree t2 under the AS model.
    The two trees must have the exact same topology and must display a 'spike' feature at all nodes.'''
    # counts the nb of spikes added, 
    # the nb of spikes substracted between the two trees, 
    # as well as the spike number in the first tree
    nplus, nmoins, spikeNb = 0, 0, 0
    for (n1, n2) in zip(t1.traverse(), t2.traverse()):
        spikeNb += n1.spike
        if n1.spike > n2.spike:
            nmoins += n1.spike - n2.spike
        elif n2.spike > n1.spike:
            nplus += n2.spike - n1.spike

    # the log probability corresponding to the distinct events
    if spikeNb > 0:
        if nplus == 0 and nmoins > 0:
            result = log(0.05) + log(1/spikeNb)
        elif nmoins == 0 and nplus > 0:
            result = log(0.95) - 1 - log( factorial(nplus - 1) )
        else:
            result = -np.inf
    else:
        if nmoins == 0 and nplus > 0:
            result = - 1 - log( factorial(nplus - 1) )
        else:
            result = -np.inf

    return result

