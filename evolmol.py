import numpy as np
from math import exp

# This file defines a model of molecular evolution (K80)
# aimed at being used along a timed tree with spike annotations.

def pi(params):
    '''The stationary distribution of the chain.'''
    return np.array([0.25, 0.25, 0.25, 0.25])

def matQ(params):
    '''The intensity matrix of the chain'''
    [alpha, beta, kappa] = params
    q = -alpha -2*beta
    v = beta
    r = alpha
    return np.array([[q, v, v, r],[v, q, r, v],[v,r,q,v],[r,v,v,q]]) 

def matP(params, time, spike):
    '''The matrix of substitution probabilities after a 
    given time and a given number of spikes'''
    [alpha, beta, kappa] = params
    v = (1 - exp(-4*beta*time)) / 4.
    r = 1./4. + 1./4. * exp(-4*beta*time) - 1./2. * exp( -2* (alpha+beta) * time)
    q = 1 - r - 2*v
    matPt = np.array([[q, v, v, r],[v, q, r, v],[v,r,q,v],[r,v,v,q]]) 
    if spike == 0:
        result = matPt
    else:
        q = 1 - kappa
        v = kappa * beta / (alpha + 2*beta)
        r = kappa * alpha / (alpha + 2*beta)
        matPs = np.array([[q, v, v, r],[v, q, r, v],[v,r,q,v],[r,v,v,q]]) 
        result = np.dot( np.linalg.matrix_power(matPs, spike), matPt )
    return result

