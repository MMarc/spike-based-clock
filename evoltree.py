import numpy as np
import nt
import evolmol

# This file contains all algorithms concerning a Markov chain running
# on a tree.

def simSeq(params, t, m):
    ''' Simulates sequences at the nodes of the tree t.
    Under a model defined in the 'evolmol' file 
    (stationary distribution 'pi' at the root and evolving as a Markov process with transition matrix 'matP'). '''
    # we add sequences on a copy of the tree
    t2 = t.copy()

    # we simulate the sequence at the root
    seq0 = nt.simSeq( evolmol.pi(params), m )
    t2.add_features(seq = seq0)

    # and then recursively the sequences in the descent
    for n in t2.iter_descendants("preorder"):
        seq1 = nt.simSeqEvol( n.up.seq, evolmol.matP(params, n.dist, n.spike) )
        n.add_features(seq = seq1)

    return t2

def getAlignment(t):
    ''' Get the alignment of the tip sequences. The tree t must display a 'seq'
    feature. '''
    al = []
    names = []
    for n in t.iter_leaves():
        al.append(n.seq)
        names.append(n.name)
    al = np.array(al)
    return names, al

def loglAl(params, t, simplifiedAl, columnCounts):
    ''' Pruning algorithm to compute recursively the log of the probability of
    the leaf sequences knowing the states at internal nodes.
    Returns the tree annotated with these conditional probabilities, together
    with the log of the probability of the observed leaf alignment. '''
    t2 = t.copy()
    nleaves, m = simplifiedAl.shape

    # Rows of the simplifiedAl correspond to sequences of successive leaves (indexed by i)
    i = 0
    for n in t2.traverse("postorder"):
        if n.is_leaf():
            # the conditional likelihood depends on the observed state at the tips
            lcrown = nt.indicSeq( simplifiedAl[i,:] )
            # and then evolves as we go to the internal extremity as
            lstem = np.dot( evolmol.matP(params, n.dist, n.spike), lcrown)
            i += 1
        else:
            # the conditional crown likelihood is the hadamard product of the conditional
            # stem likelihoods of the childs
            lcrown = np.ones((4,m))
            for child in n.get_children():
                lcrown *= child.lstem
            # and this evolves as we go to the internal extremity of the branch as
            lstem = np.dot( evolmol.matP(params, n.dist, n.spike), lcrown)
        n.add_features(lstem = lstem, lcrown = lcrown)

    # the log probability of the observed alignment is the matrix product with the row
    # vector of the stationary distribution at the root, and the column vector of column counts
    logL = np.dot( np.log( np.dot(evolmol.pi(params), t2.lstem) ), columnCounts)
    return t2, logL

def eAlChange(params, t, simplifiedAl, columnCounts):
    ''' Annotates each branch of the tree with a new feature called "value",
    which corresponds to the difference between the expected number of
    substitutions knowing the alignment, and the expected number of substitutions
    without knowing the alignment. '''
    # we first need the pruning algo until reaching the root
    nleaves, m = simplifiedAl.shape
    t2, logL = loglAl(params, t, simplifiedAl, columnCounts)

    # vector with likelihood at each site, useful to derive eAl
    l = np.dot(evolmol.pi(params), t2.lcrown)
    L = np.outer(np.ones(4), l)
    pirep = np.outer( evolmol.pi(params), np.ones(m) )
    
    # then a second depth traversal allows to compute 'lup', the conditional likelihood
    # as if the tree were rooted at the current node
    t2.add_features(lup = np.ones((4,m)) )
    for n in t2.iter_descendants("preorder"):
        lupstem = n.up.lup * n.up.lcrown / n.lstem
        lupcrown = np.dot( evolmol.matP(params, n.dist, n.spike), lupstem)
        n.add_features(lup = lupcrown)

        # We sum the expected number of changes knowing the alignment
        eChangeKnowingA = 0
        for k in range(0,m):
            for i in range(0,4):
                for j in range(0,4):
                    if i != j:
                        # what follows corresponds to the computation of eAl, the expectation
                        # of the number of substitutions at each site.
                        # for i,j,k, this is the quantity p(Xw1^k = i, Xw2^k = j | A)
                        eChangeKnowingA += columnCounts[k] * evolmol.matP(params, n.dist, n.spike)[i,j] * n.lcrown[j,k] * evolmol.pi(params)[i] * lupstem[i,k] / l[k]

        # And we subtract the expected number of change, without knowing the alignment
        mprime = sum(columnCounts)
        eChange = 0
        for i in range(0,4):
            for j in range(0,4):
                if i != j:
                    eChange += mprime * evolmol.matP(params, n.dist, n.spike)[i, j] * evolmol.pi(params)[i]

        # We then annotate the node with the difference between these two expectations
        n.add_features(value = eChangeKnowingA - eChange)

    return t2, logL

