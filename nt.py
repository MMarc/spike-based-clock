# One nucleotide at a time.
# 0: A
# 1: T
# 2: C
# 3: G

import numpy as np

# This file contains generic algorithms for nucleotide
# and sequences simulations and comparisons.

def ntToStr(nt):
    if nt == 0:
        char = 'A'
    elif nt == 1:
        char = 'T'
    elif nt == 2:
        char = 'C'
    elif nt == 3:
        char = 'G'
    else:
        print('Error, non-existing nucleotide value')
        char = 'X'
    return char

def strToNt(char):
    if char == 'A' or char == "a":
        nt = 0
    elif char == 'T' or char == "t":
        nt = 1
    elif char == 'C' or char == "c":
        nt = 2
    elif char == 'G' or char == "g":
        nt = 3
    else:
        print('Error, this nucleotide letter is unknown:', char)
        nt = 10
    return nt

def seqToStr(seq):
    char = ''
    for i in seq:
        char += ntToStr(i)
    return char

def printNt(nt):
    print(ntToStr(nt))

def printSeq(seq):
    for i in seq:
        printNt(i)

def indicNt(nt):
    if nt == 0:
        result = np.array([1., 0., 0., 0.])
    elif nt == 1:
        result = np.array([0., 1., 0., 0.])
    elif nt == 2:
        result = np.array([0., 0., 1., 0.])
    elif nt == 3:
        result = np.array([0., 0., 0., 1.])
    else:
        result = "Error: a nucleotide should be an int between 0 and 3."
    return result

def indicSeq(seq):
    m = seq.size
    result = np.zeros((4,m))
    for i in range(0,m):
        result[:,i] = indicNt( seq[i] )
    return result

def simNt(law):
    s = sum(law)
    if law.size != 4 or s < 0.999999 or s > 1.00001:
        result = "Error: the law of a given nucleotide should be a line vector with four elements, summing to one."
    else:
        alea = np.random.random()
        i, cumsum = 0, law[0]
        while alea > cumsum and i < 4:
            i += 1
            cumsum += law[i]
        result = i
    return result

def simSeq(law, m):
    seq = np.zeros(m)
    for i in range(0,m):
        seq[i] = simNt(law)
    return seq

def simSeqEvol(seq0, matP):
    if matP.shape != (4,4):
        result = "Error: the transition matrix should be 4*4."
    else:
        seq1 = np.zeros( seq0.size )
        for i in range(0, seq0.size):
            seq1[i] = simNt( matP[int(seq0[i]), :] )
    return seq1

def simplifyAlignment(seqArray, cmpColumns):
    n,m = np.shape(seqArray)
    # the first column is new for sure
    colList, colNb = [seqArray[:,0]], [1]

    for i in range(1, m):
        j = 0
        EqNotFound = True
        # we compare the column pattern (i) to all previously observed column-patterns
        while j < len(colList) and EqNotFound:
            # if they are equivalent, we add one count, and we stop comparing
            if cmpColumns(seqArray[:,i], colList[j]):
                colNb[j] += 1
                EqNotFound = False
            # otherwise we compare the column pattern to the next previously observed pattern
            j += 1

        # if the column pattern had not been observed before, we add it to the pool
        if EqNotFound:
            colList.append( seqArray[:,i] )
            colNb.append(1)

    # we convert the list of columns into an array,
    # that we transpose to make it look like the input al
    simplifiedAl = np.transpose(np.array(colList))
    return simplifiedAl, colNb


def cmpColumnsJC69(col1, col2):
    '''Compares the patterns of nucleotide in two columns.
    e.g. AAAT is equivalent to CCCG under the JC69 model.'''
    translation12, translation21 = -1 * np.ones(4), -1 * np.ones(4)
    patternEq = True
    i = 0
    while patternEq and i < col1.size:
        # if the two nt are met for the first time
        if translation12[int(col1[i])] == -1 and translation21[int(col2[i])] == -1:
            translation12[int(col1[i])] = col2[i]
            translation21[int(col2[i])] = col1[i]
        # if at least one of them has been met before and translated to a different one
        elif translation12[int(col1[i])] != col2[i] or translation21[int(col2[i])] != col1[i]:
            patternEq = False
        # otherwise we continue to compare the next two nt of the two columns
        i += 1
    return patternEq

def cmpColumnsBasic(col1, col2):
    '''The columns are equivalent when they are composed of exactly the same nucleotides.'''
    return np.all(col1 == col2)

