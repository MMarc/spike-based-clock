import numpy as np
import os

def meanSpike(folderName, fileName, t, nBurnin):
    '''Open the results_spikes.txt file, average the number of spikes
    over the whole distribution. And add the mean spike value 
    to the spike attribute of a tree.'''

    # We open the file containing spike counts and transform it into an array
    os.chdir(folderName)
    a = np.loadtxt(fileName, skiprows=nBurnin)

    # We average over the lines of the array
    mean_spikes = np.mean(a, axis=0)

    # We place these average numbers along the tree
    t_mean = t.copy()
    nn = 0
    for n in t_mean.iter_descendants("postorder"):
        n.add_features(spike = mean_spikes[nn])
        nn += 1

    # We record this tree in a file, and return it
    with open("result_tree_with_mean_spikes.txt", "w") as fichier:        
        fichier.write(t_mean.write(features=["spike"], format=1) + '\n')
    
    os.chdir('..')
    return t_mean 

